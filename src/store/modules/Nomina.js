import axios from "axios"

const state = {
    distributors: []
}

const actions = {
    async getDistributors({ commit }, [search]) {
        const dateNomina = `${search.anio}-${search.mes}`
        const response = await axios.get(
            "admin/totalmanifestos?dateNomina=" + dateNomina 
        );
        if (response) {
            commit("SET_DISTRIBUTORS", response.data.data);
        }
    }
};

const mutations = {
    SET_DISTRIBUTORS(state, data) {
        state.distributors = []
        const result = data
        for(let i = 0; i < result.length; i++) {
            let valDato = state.distributors.filter((d) => d.id == result[i]['id'])
            //console.log(valDato, result[i]['id'], valDato.length > 0)
            if(valDato.length > 0) {
                //console.log("encontró", result[i]['name'], result[i]['id'])
                let j = state.distributors.findIndex((d) => d.id == result[i]['id'])
                state.distributors[j]['tlote'] = state.distributors[j]['tlote'] + 1
                if(result[i]['total'] == 10000) { state.distributors[j]['tlotedeliver'] =  state.distributors[j]['tlotedeliver'] += 1}
                state.distributors[j]['tnomina'] = 
                result[i]['total'] == 10000 
                ? state.distributors[j]['tnomina'] + Number(result[i]['total'])
                : state.distributors[j]['tnomina'] - Number(result[i]['total'])
            } else {
                state.distributors.push({
                    'id': result[i]['id'],
                    'name': result[i]['name'],
                    'tlote': 1,
                    'tlotedeliver': result[i]['total'] == 10000 ?  1 :   0, 
                    'tnomina': result[i]['total'] == 10000 
                        ? 50000 + Number(result[i]['total'])
                        : 50000 - Number(result[i]['total'])
                })
            }
        } 
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}