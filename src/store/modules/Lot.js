import axios from "axios"

const state = {
    lots: [],
}

const actions = {
    async getList({ commit }, [pageNumber, search]) {
        const response = await axios.get(
            "admin/lots?page=" + pageNumber + "&search=" + search
        );
        if (response) {
            commit("SET_LOTS", response.data.data);
        }
    }
};

const mutations = {
    SET_LOTS(state, data) {
        state.lots = data
    },

    SetCurrentPage(state, data) {
        state.lots.current_page = data;
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}