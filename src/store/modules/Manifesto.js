import axios from "axios"

const state = {
    manifestos: []
}

const actions = {
    async getList({ commit }, [pageNumber, search]) {
        const response = await axios.get(
            "admin/manifestos?page=" + pageNumber + "&search=" + search
        );
        if (response) {
            commit("SET_MANIFESTOS", response.data.data);
        }
    },

    async updateManifesto(context, details) {
        const response = await axios.put('admin/manifesto/update', {
            detail: details
        })
        return response
    }
};

const mutations = {
    SET_MANIFESTOS(state, data) {
        state.manifestos = data
    },

    SetCurrentPage(state, data) {
        state.manifestos.current_page = data;
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}