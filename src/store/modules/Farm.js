import axios from "axios"

const state = {
    farms: [],
}

const actions = {
    async getList({ commit }, [pageNumber, search]) {
        const response = await axios.get(
            "admin/farms?page=" + pageNumber + "&search=" + search
        );
        if (response) {
            commit("SET_FARMS", response.data.data);
        }
    }
};

const mutations = {
    SET_FARMS(state, data) {
        state.farms = data
    },

    SetCurrentPage(state, data) {
        state.farms.current_page = data;
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}