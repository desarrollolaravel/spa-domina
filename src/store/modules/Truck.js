import axios from "axios"

const state = {
    trucks: [],
}

const actions = {
    async getList({ commit }, [pageNumber, search]) {
        const response = await axios.get(
            "admin/trucks?page=" + pageNumber + "&search=" + search
        );
        if (response) {
            commit("SET_TRUCKS", response.data.data);
        }
    }
};

const mutations = {
    SET_TRUCKS(state, data) {
        state.trucks = data
    },

    SetCurrentPage(state, data) {
        state.trucks.current_page = data;
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}