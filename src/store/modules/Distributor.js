import axios from "axios"

const state = {
    distributors: [],
}

const actions = {
    async getList({ commit }, [pageNumber, search]) {
        const response = await axios.get(
            "admin/distributors?page=" + pageNumber + "&search=" + search
        );
        if (response) {
            commit("SET_DISTRIBUTORS", response.data.data);
        }
    }
};

const mutations = {
    SET_DISTRIBUTORS(state, data) {
        state.distributors = data
    },

    SetCurrentPage(state, data) {
        state.distributors.current_page = data;
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}