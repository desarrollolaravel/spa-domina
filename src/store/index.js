import Vue from 'vue'
import Vuex from 'vuex'
import login from './modules/Login'
import farm from './modules/Farm'
import lot from './modules/Lot'
import truck from './modules/Truck'
import distributor from './modules/Distributor'
import manifesto from './modules/Manifesto'
import nomina from './modules/Nomina'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    login,
    farm,
    lot,
    truck,
    distributor,
    manifesto,
    nomina
  }
})
