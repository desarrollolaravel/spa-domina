import Vue from 'vue'
import VueRouter from 'vue-router'
import login from '../views/Login.vue'
import store from '../store'


Vue.use(VueRouter)

const routes = [
  {
    path: '*',
    redirect: 'login',
  },
  {
    path: '/',
    redirect: 'login',
  },
  {
    path: '/login',
    name: 'login',
    component: login
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('../views/Home.vue'), 
    beforeEnter: (to, from, next) => {
        if(!store.getters['login/token']){
          router.push("/login")
        }
        next()
    }, 
    children: [
      {
        path: "/dasboard",
        name: "dasboard",
        component: () => import("../components/Dashboard.vue"),
      },
      {
        path: "/farms",
        name: "farms",
        component: () => import("../components/Farm.vue"),
      },
      {
        path: "/lots",
        name: "lots",
        component: () => import("../components/Lot.vue")
      },
      {
        path: "/trucks",
        name: "trucks",
        component: () => import("../components/Truck.vue")
      },
      {
        path: "/distributors",
        name: "distributors",
        component: () => import("../components/Distributor.vue")
      },
      {
        path: "/manifestos",
        name: "manifestos",
        component: () => import("../components/Manifesto.vue")
      },
      {
        path: "/nomina",
        name: "nomina",
        component: () => import("../components/Nomina.vue")
      }
    ],
  }
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
})

export default router
